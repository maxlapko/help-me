Helpme::Application.routes.draw do
  get "site/index"
  devise_for :users, :controllers => { :registrations => :registrations }

  resources :users, :only => [:show]
  resources :posts do
    member do
      put :cover
    end
    resources :images, :only => [:create, :destroy]
  end
  root :to => 'site#index'
end
