class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.references :user
      t.integer :cover_id

      t.timestamps
    end
    add_index :posts, :user_id
    add_index :posts, :cover_id
  end
end
