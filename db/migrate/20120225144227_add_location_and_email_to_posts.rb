class AddLocationAndEmailToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :location, :string
    add_column :posts, :email, :string
    add_index :posts, :location
  end
end
