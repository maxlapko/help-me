class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references :post
      t.references :user
      t.string :title
      t.string :url
      t.integer :order, :default => 0

      t.timestamps
    end
    add_index :images, :user_id
    add_index :images, :post_id
    add_index :images, :order
  end
end
