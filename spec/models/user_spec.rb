require 'spec_helper'

describe User do
  it "uses full name firstname + lastname" do
      Factory(:user, :first_name => "test", :last_name => "test2").full_name.should eq("test test2")
  end
end
