module ControllerMacros
  def login_user(user = nil)
    @user = user || Factory.create(:user)
    page.driver.post user_session_path, :user => {:email => @user.email, :password => @user.password}
  end

  def logout_user
    page.driver.delete destroy_user_session_path
  end
end