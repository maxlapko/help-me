Factory.define :user do |f|
  f.sequence(:email)      { |n| "foo#{n}@example.com" }
  f.sequence(:first_name) { |n| "test#{n}" }
  f.sequence(:last_name)  { |n| "test#{n}" }
  f.password              'qwerty123'
end

Factory.define :post do |f|
  f.title            'Need help'
  f.content          'Hello world.'
  f.location         'Sevastopol'
  f.sequence(:email) { |n| "test#{n}@example.com" }
  f.user             { |c| c.association(:user) }
  f.cover            { |c| c.association(:image) }
end

Factory.define :image do |f|
  f.title 'Foo Bar'
  f.url   'http://cs11116.vk.com/u2252921/a_32ccdaf6.jpg'
  f.post  { |c| c.association(:post) }
end