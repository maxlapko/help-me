#-*- coding: utf-8 -*-
require 'spec_helper'

describe PostsController do

  #it 'posts page should shows problems' do
  #  Factory.create(:post, :title => 'Batman return to back', :content => 'Тест Бетмен')
  #  Factory.create(:post, :title => 'Ватман вернулся', :content => 'Тест Бетмен', :user => user)
  #  visit posts_path
  #  page.should have_content('Есть проблема')
  #  page.should have_content('Тест Бетмен')
  #  page.should have_content('Тест Бетмен')
  #end

  #it 'show one problem' do
  #  post = Factory.create(:post, :title => 'Супер мен', :content => 'Просит помощь', :location => 'Севастополь', :user => Factory.create(:user))
  #  visit post_path(post)
  #  page.should have_content('Севастополь')
  #  page.should have_content('Супер мен')
  #  page.should have_content('Просит помощь')
  #  page.should_not have_content('Редактировать')
  #end

  context "when user logged in" do
    before(:each) { login_user }
    after(:each) { logout_user }

    it 'create post' do
      visit new_post_path
      response.should be_success
      click_on 'Создать'
      page.should have_content('не может быть пустым')
      fill_in 'Название', :with => 'Batman'
      fill_in 'Содержание', :with => 'test'
      fill_in 'Теги', :with => 'test adam , eva'
      click_on 'Создать'
      page.current_path.should eq(post_path(Post.last))
      Post.last.tags.count == 2
      Post.last.tags.map(&:name) == ['test adam', 'eva']
    end

  end
end
