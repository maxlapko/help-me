# -*- coding: utf-8 -*-
require 'spec_helper'

describe 'Posts request' do
  include Devise::TestHelpers

  it 'redirect to login page if user is not authorized' do
    visit new_post_path
    page.should have_content('Вам необходимо войти в систему')
    end

  it 'check button "have problem"' do
    visit posts_path
    page.should have_content('Есть проблема')
  end

  it 'create post' do
    @user = Factory.create(:user)
    sign_in @user
    visit new_post_path
    response.should be_success
    click_on "Создать"
    page.should have_content("Invalid Fields")
    fill_in "Название", :with => "Batman"
    fill_in "Содержание", :with => "test"
    fill_in "Теги", :with => "test adam , eva"
    click_on "Создать"

    page.current_path.should eq(post_path(Post.last))
    Post.last.tags.count == 2
    Post.last.tags.map(&:name) == ['test adam', 'eva']
  end
end
