json.posts @posts do |json, post|
  json.(post, :id, :title, :location, :tag_names )
  json.url post_path(post)
  json.cover_url cover_url(post)
  json.created_at Russian::strftime(post.created_at, '%d %b в %H:%M')
end

json.next_page @posts.next_page.to_s