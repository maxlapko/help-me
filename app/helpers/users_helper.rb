module UsersHelper
  def avatar(user, size = '50x50')
    if user.profile.avatar.present?
      image_tag user.profile.avatar_url(:profile_avatar), :alt => user.full_name, :size => size
    else
      image_tag '/assets/default_avatar_150.png', :alt => user.full_name, :size => size
    end
  end
end
