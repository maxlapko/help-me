module PostsHelper
  def cover(post, size = '50x50')
    image_tag cover_url(post), :alt => post.title, :size => size
  end

  def cover_url(post)
    if post.cover and post.cover.url.present?
      post.cover.url_url(:profile_avatar)
    else
      '/assets/default_avatar_150.png'
    end
  end

  def view_tags(post)
    post.tags.map{ |tag| link_to tag.name, posts_path(:tag_id => tag.id ) }.join(', ')
  end
end
