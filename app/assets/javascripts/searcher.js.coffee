jQuery ->
  new Searcher() if $('#search_form').length > 0

class Searcher
  constructor: ->
    @sending = false
    @form = $('#search_form')
    @template = $('#post_template').html()
    $('#q, #location').keydown (e)=>
      @change()

  change: ->
    @clearInterval()
    @sending = setTimeout @process, 500, @

  process: (parent)->
    q = $.trim $('#q').val()
    location = $.trim $('#location').val()
    if q.length > 2 || location.length > 2
      $.getJSON parent.form.attr('action'), { q: q, location: location }, (rsp) =>
        $('#posts').html parent.render(rsp)
    else if q.length == 0 && location.length == 0
      true

  render: (rsp)->
    posts = ''
    for post in rsp.posts
      posts += Mustache.render @template, post
    posts

  clearInterval: ->
    clearTimeout(@sending) if @sending
