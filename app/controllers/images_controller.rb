class ImagesController < ApplicationController
  before_filter :authenticate_user!, :only => [:create, :destroy ]
  before_filter :set_post_and_image

  def create
    respond_to do |format|
      unless @image.save
        flash[:error] = t('Photo could not be uploaded')
      end
      format.js do
        render :text => render_to_string(:partial => 'images/image', :locals => {:image => @image})
      end
    end
  end

  def destroy
    respond_to do |format|
      unless @image.destroy
        flash[:error] = t('Photo could not be deleted')
      end
      format.js
    end
  end

  private

  def set_post_and_image
    @post = current_user.posts.find(params[:post_id])
    raise ActiveRecord::RecordNotFound unless @post
    @image = params[:id] ? @post.images.find(params[:id]) : @post.images.build(params[:image])
  end
end
