#-*- coding: utf-8 -*-
class PostsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  load_resource

  respond_to :html, :json, :js
  def index
    @posts = Post.recent.includes(:cover, :tags).page(params[:page]).per_page(20)
    prepare_search
    respond_with @posts
  end

  def show
    @post = Post.find params[:id]
  end

  def new
    @post = Post.new
    respond_with @post
  end

  def edit
    @post = current_user.posts.find params[:id]
  end

  def create
    @post = current_user.posts.build params[:post]

    respond_to do |format|
      if @post.save
        format.html { redirect_to(@post, :notice => 'Запись успешно создана.') }
        format.xml  { render :xml => @post, :status => :created, :location => @post }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @post.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to(@post, :notice => 'Запись успешно отредактирована.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @post.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to(posts_url) }
      format.xml  { head :ok }
    end
  end

  def cover
    @post = current_user.posts.find params[:id]
    @post.cover = @post.images.find params[:image_id]
    @post.save :validate => false
    respond_to do |format|
      format.js
    end
  end

  private

  def prepare_search
    if params[:q].present?
      @posts = @posts.primitive_search %w[title content], params[:q]
    end
    if params[:location].present?
      @posts = @posts.primitive_search %w[location], params[:location]
    end
  end

end
