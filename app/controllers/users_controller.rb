class UsersController < ApplicationController
  load_resource
  def show
    @user = User.find params[:id]
  end
end
