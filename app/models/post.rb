class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :cover, :class_name => 'Image', :foreign_key => 'cover_id'
  has_many :images, :dependent => :destroy
  has_many :taggings, :dependent => :destroy
  has_many :tags, :through => :taggings

  scope :recent, order('created_at DESC')
  scope :tagged, lambda { |tag_id| tag_id ? joins(:taggings).where(:taggings => {:tag_id => tag_id}) : scoped }

  validates_presence_of :title, :content
  validates_format_of :email, :with => /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i, :allow_blank => true

  def tag_names=(names)
    self.tags = Tag.with_names(names.strip.split(/,+/).map{ |n| n.strip }.uniq)
  end

  def tag_names
    tags.map(&:name).join(', ')
  end

  def self.primitive_search(fields, query)
    where(primitive_search_conditions(fields, query))
  end

  def self.primitive_search_conditions(fields, query)
    query.split(/\s+/).map do |word|
      '(' + fields.map { |col| "#{col} LIKE #{sanitize('%' + word.to_s + '%')}" }.join(' OR ') + ')'
    end.join(" AND ")
  end
end
