class Profile < ActiveRecord::Base
  mount_uploader :avatar, ImageUploader
  belongs_to :user
  validates_length_of :description, :maximum => 500

end
