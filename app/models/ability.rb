class Ability
  include CanCan::Ability

  def initialize(current_user)
    current_user ||= User.new # guest user
    can :read, :all
    can :update, User do |user|
      user == current_user
    end

    can :update, Post do |post|
      post.try(:user) == current_user
    end
  end
end
