class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable,:omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  has_many :posts
  has_one :profile
  accepts_nested_attributes_for :profile
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :profile_attributes

  validates_presence_of :first_name, :last_name
  after_create :create_profile



  def full_name
    self.first_name + ' ' + self.last_name
  end

  private

  def create_profile
    self.create_profile!
  end

end
